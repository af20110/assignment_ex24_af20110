import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Assignment_ex24_af20110 {
    private JPanel root;
    private JLabel topLabel;
    private JButton gyudonButton;
    private JButton negitamaGyudonButton;
    private JButton cheeseGyudonButton;
    private JButton kimutiGyudonButton;
    private JButton butadonButton;
    private JButton gyukarubidonButton;
    private JTextPane receivedInfo;
    private JButton checkOutButton;
    private JTextPane totalprice;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment_ex24_af20110");
        frame.setContentPane(new Assignment_ex24_af20110().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    void order(String Food ,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+Food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0){
            JOptionPane.showMessageDialog (null ,
                    "Thank you for ordering "+Food+"! It will be served as soon as possible");
            String currentText = receivedInfo.getText();
            receivedInfo.setText(currentText +Food+"\n");
            total+=price;
            totalprice.setText("total "+total+" yen");
        }
    }

    int total = 0;

    public Assignment_ex24_af20110() {
        gyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("gyudon" ,500);
            }
        });
        gyudonButton.setIcon(new ImageIcon( this.getClass().getResource("nikudaku-gyu-don-200x200.jpg")
        ));
        negitamaGyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("negitamaGyudon" ,520);
            }
        });
        negitamaGyudonButton.setIcon(new ImageIcon( this.getClass().getResource("negitama-gyu-don_202005-200x200.jpg")
        ));
        cheeseGyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("cheeseGyudon" ,540);
            }
        });
        cheeseGyudonButton.setIcon(new ImageIcon( this.getClass().getResource("cheese_gyu_don-200x200.jpg")
        ));
        kimutiGyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("kimutiGyudon" ,530);
            }
        });
        kimutiGyudonButton.setIcon(new ImageIcon( this.getClass().getResource("kimuchi_gyu_don-1-200x200.jpg")
        ));
        butadonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("butadon" ,400);
            }
        });
        butadonButton.setIcon(new ImageIcon( this.getClass().getResource("buta-don-gomanashi-200x200.jpg")
        ));
        gyukarubidonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("gyukarubidon" ,600);
            }
        });
        gyukarubidonButton.setIcon(new ImageIcon( this.getClass().getResource("gyu-karubi-don_202005-200x200.jpg")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null ,
                        "Would you like to check out?",
                        "Check out Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if (confirmation == 0) {
                    int confirmation1=JOptionPane.showConfirmDialog(null ,
                            "Do you want takeout?",
                            "Takeout Confirmation1",
                            JOptionPane.YES_NO_OPTION
                    );
                    if (confirmation1 == 0) {
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " + (int)(total*1.08) + "yen");
                        total = 0;
                        receivedInfo.setText("");
                        totalprice.setText("total 0yen");
                    }
                    else if (confirmation1 == 1){
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " + (int)(total*1.1) + "yen");
                        total = 0;
                        receivedInfo.setText("");
                        totalprice.setText("total 0yen");
                    }
                }

            }
        });
    }
}
